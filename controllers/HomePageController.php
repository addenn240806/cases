<?php

use services\Cases;
use services\SearchPatient;

require_once "services/Cases.php";
require_once "services/SearchPatient.php";

/**
 * Class HomePageController
 */
class HomePageController {

  /**
   * @var Cases;
   */
  protected $cases;

  /**
   * @var SearchPatient;
   */
  protected $searchPatient;

  /**
   * @var Database;
   */
  protected $database;

  /**
   * HomePageController constructor.
   */
  public function __construct() {
    global $database;

    $this->cases = new Cases();
    $this->searchPatient = new SearchPatient();
    $this->database = $database;
  }

  /**
   * Init variables for template.
   *
   * @var array
   */
  public $variables = [
    'error' => '',
    'success' => '',
    'surname' => '',
    'firstName' => '',
    'lastName' => '',
    'birthdate' => '',
    'pser' => '',
    'pnum' => '',
  ];

  /**
   * Controller callback.
   */
  public function launch() {
    // Process form if it's submitted.
    if (isset($_POST['casesFormSubmit'])) {
      $this->processForm();
    }
    // Show success form message if it exists.
    if (!empty($_SESSION['cases']['success']) && empty($_POST['casesFormSubmit'])) {
      $this->variables['success'] = $_SESSION['cases']['success'];
      unset($_SESSION['cases']['success']);
    }
    $variables = $this->variables;
    require_once 'templates/case-sign-form.php';
  }

  /**
   * Execute cases form logic.
   */
  private function processForm() {
    $fields = ['surname', 'firstName', 'lastName', 'birthdate', 'pnum', 'pser'];
    foreach ($fields as $field) {
      $this->variables[$field] = $_POST[$field] ?? '';
      if (empty($this->variables[$field])) {
        $this->variables['error'] = 'You should fill all fields.';
      }
    }

    // If there are no errors - all fields are filled.
    if (empty($this->variables['error'])) {
      // Data for search_patient request.
      $patient = [
        'request' => [
          'surname' => $this->variables['surname'],
          'firstname' => $this->variables['firstName'],
          'lastname' => $this->variables['lastName'],
          'birthdate' => $this->variables['birthdate'],
          'pd_ser' => $this->variables['pser'],
          'pd_numb' => $this->variables['pnum'],
        ]
      ];

      $lastCaseId = NULL;
      $result = $this->database->select('cases', ['id'], 'id', 'DESC', 1);
      if (!empty($result)) {
        $result = array_shift($result);
        $lastCaseId = $result['id'];
      }
      if ($lastCaseId) {
        $caseId = $lastCaseId + 1;
        // Data for submit case request.
        $case = [
          'request' => [
            'patient_id' => '',
            'call_id' => $caseId,
            'case_type' => 4,
            'case_status' => 5,
            'sender_mo' => 6703,
            'call_card_num' => 671,
            'call_date' => '2018-07-13T20:55:59',
            'call_address' => ' , , 35, 3, 9',
            'age_unit' => '1',
            'gender' => 'male',
            'age' => '25',
            'surname' => $this->variables['surname'],
            'firstname' => $this->variables['firstName'],
            'lastname' => $this->variables['lastName'],
            'birthdate' => '',
            'pd_ser' => '',
            'pd_numb' => '',
          ]
        ];

        $response = $this->searchPatient->searchPatient($patient);
        // If response has id it means user is registered in the system.
        // If not fill his/her data for submit request.
        if (!empty($response->id)) {
          $case['patient_id'] = $response->id;
        }
        else {
          $case['request'] = array_merge($case['request'], $patient['request']);
        }

        $response = $this->cases->submit($case);
        if (empty($response->response->error_msg)) {
          $_SESSION['cases']['success'] = 'A new case has been submitted successfully.';
          $fields = ['id', 'surname', 'firstname', 'lastname', 'birthdate', 'pdser', 'pdnumb'];
          $info = $patient['request'];
          $data = [$caseId, $info['surname'], $info['firstname'], $info['lastname'], $info['birthdate'], $info['pd_ser'], $info['pd_numb']];
          $this->database->insert('cases', $fields, $data);
          header('location: /');
        }
        else {
          $this->variables['error'] = "A new case can not be submitted.<br>" . $response->response->error_msg;
        }
      }
    }
  }
}
