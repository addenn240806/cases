<?php

/**
 * Class MainController
 */
class MainController {

  /**
   * Controller callback.
   */
  public function launch() {
    require_once 'templates/main.html';
  }
}