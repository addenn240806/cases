<?php

require_once 'interfaces/DatabaseInterface.php';

use interfaces\DatabaseInterface;

/**
 * Class Database
 */
class Database implements DatabaseInterface {

  /**
   * @var \PDO
   */
  protected $pdo;

  /**
   * {@inheritdoc}
   */
  public function __construct(string $host, string $db, string $user, string $pass) {
    $this->prepareDatabase($host, $db, $user, $pass);
    $dsn = "mysql:host={$host};dbname={$db}";
    $opt = [
      \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
      \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
      \PDO::ATTR_EMULATE_PREPARES => false,
    ];
    $this->pdo = new \PDO($dsn, $user, $pass, $opt);
    $this->prepareTables();
  }

  /**
   * Create database if it does not exist.
   *
   * @param string $host
   * @param string $db
   * @param string $user
   * @param string $pass
   */
  private function prepareDatabase(string $host, string $db, string $user, string $pass) {
    $pdo = new \PDO("mysql:host={$host}", $user, $pass);
    $pdo->query("CREATE DATABASE IF NOT EXISTS `$db`");
  }

  /**
   * Creates necessary tables in database.
   */
  private function prepareTables() {
    $query ="CREATE TABLE IF NOT EXISTS `cases`(
      id INT AUTO_INCREMENT PRIMARY KEY,
      surname VARCHAR(250) NOT NULL, 
      firstname VARCHAR(250) NOT NULL,
      lastname VARCHAR(250) NOT NULL,
      birthdate DATE NOT NULL,
      pdser VARCHAR(250) NOT NULL,
      pdnumb VARCHAR(250) NOT NULL);";
    $this->pdo->exec($query);
  }

  /**
   * Insert query.
   *
   * @param string $table
   * @param array $fields
   * @param array $data
   */
  public function insert(string $table, array $fields, array $data) {
    $columns = "(`" . implode("`,`", $fields) . "`)";
    $values = "('" . implode("','", $data) . "')";
    $query = "INSERT INTO `{$table}` {$columns} VALUES {$values};";
    $this->pdo->query($query);
  }

  /**
   * Select query.
   *
   * @param string $table
   * @param array $fields
   * @param string $orderBy
   * @param string $orderByType
   * @param null|integer $limit
   * @return array
   */
  public function select(string $table, array $fields, $orderBy = '', $orderByType = 'DESC', $limit = NULL) {
    $columns = '`' . implode('`,`', $fields) . '`';
    $query = "SELECT $columns FROM `{$table}`";

    if ($orderBy) {
      $query .= "ORDER BY `{$orderBy}` {$orderByType}";
    }

    if ($limit) {
      $query .= " LIMIT {$limit} ";
    }

    $query .= ";";

    return $this->pdo->query($query)->fetchAll();
  }

}
