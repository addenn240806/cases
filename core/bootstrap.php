<?php
require_once 'Database.php';
require_once 'route.php';
require_once 'settings.php';

global $database;
$database = new Database($database['host'], $database['database'], $database['user'], $database['password']);

if (empty($_SESSION)) {
  session_start();
}
$route = new Route();
$route->start();