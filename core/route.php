<?php

/**
 * Class Route
 */
class Route {

  /**
   * Basic controller.
   *
   * @var string
   */
  private $mainController = 'MainController';

  /**
   * Routing paths.
   *
   * @var array
   */
  private $paths = [
    '/' => 'HomePageController'
  ];

  /**
   * Routing init.
   */
  public function start() {
    $uri = $_SERVER['REQUEST_URI'];

    if (!empty($this->paths[$uri])) {
      $this->launchController($this->mainController);
      $this->launchController($this->paths[$uri]);
    }
    else {
      Route::ErrorPage404();
    }
  }

  /**
   * Launch controller logic.
   *
   * @param $controllerName
   */
  private function launchController($controllerName) {
    include "controllers/{$controllerName}.php";
    $controller = new $controllerName;
    $controller->launch();
  }

  /**
   * 404 page.
   */
  private function ErrorPage404() {
    header('HTTP/1.1 404 Not Found');
    header("Status: 404 Not Found");
  }
}