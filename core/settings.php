<?php
// This file should not be under git.
$database = [
  'host' => 'localhost',
  'database' => 'bars',
  'user' => 'newuser',
  'password' => 'password',
];

global $show_errors;
$show_errors = TRUE;