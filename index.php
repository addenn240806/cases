<?php
global $show_errors;
if ($show_errors) {
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
}
require_once 'core/bootstrap.php';
