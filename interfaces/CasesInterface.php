<?php

namespace interfaces;

interface CasesInterface {

  /**
   * CasesInterface constructor.
   */
  public function __construct();

  /**
   * Runs soap query to create new case.
   *
   * @param array $request
   *   Request data.
   *
   * @return mixed
   */
  public function submit(array $request);

}
