<?php

namespace interfaces;

interface DatabaseInterface {
  /**
   * DatabaseInterface constructor.
   * @param string $host
   * @param string $db
   * @param string $user
   * @param string $pass
   */
  public function __construct(string $host, string $db, string $user, string $pass);

}
