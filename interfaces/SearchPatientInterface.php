<?php

namespace interfaces;

interface SearchPatientInterface {

  /**
   * SearchPatientInterface constructor.
   */
  public function __construct();

  /**
   * Runs soap query to search patient.
   *
   * @param array $patient
   *   Patient data for query.
   *
   * @return mixed
   */
  public function searchPatient(array $patient);

}
