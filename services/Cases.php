<?php

namespace services;

require_once 'interfaces/CasesInterface.php';

use interfaces\CasesInterface;

/**
 * Class Cases
 * @package services
 */
class Cases implements CasesInterface {

  /**
   * @var SoapClient
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->client = new \SoapClient('http://med-demo.bars-open.ru/med2/webservice/soap/cmp/cases?wsdl', [
      'keep_alive' => true,
      'trace' => 1,
      'encoding' =>'UTF-8',
      'compression' => SOAP_COMPRESSION_ACCEPT,
      'exceptions' => true,
      'cache_wsdl' => WSDL_CACHE_NONE,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array $request) {
    return $this->client->submit($request);
  }

}
