<?php

namespace services;

require_once 'interfaces/SearchPatientInterface.php';

use interfaces\SearchPatientInterface;

/**
 * Class SearchPatient
 * @package services
 */
class SearchPatient implements SearchPatientInterface {

  /**
   * @var SoapClient
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->client = new \SoapClient('http://med-demo.bars-open.ru/med2/webservice/soap/cmp/search_patient?wsdl', [
      'keep_alive' => true,
      'trace' => 1,
      'encoding' =>'UTF-8',
      'compression' => SOAP_COMPRESSION_ACCEPT,
      'exceptions' => true,
      'cache_wsdl' => WSDL_CACHE_NONE,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function searchPatient(array $patient) {
    $response = [];
    $result = $this->client->search_patient($patient);
    if (!empty($result->response->agent)) {
      $response = $result->response->agent;
    }

    return $response;
  }

}
