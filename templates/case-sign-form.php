<div class="container">
  <div class="border shadow-none p-4 mb-4 bg-light w-50 p-3">
    <?php if (!empty($variables['error'])): ?>
      <div class="alert alert-danger" role="alert">
        <?php print $variables['error']; ?>
      </div>
    <?php endif; ?>

    <?php if (!empty($variables['success'])): ?>
      <div class="alert alert-success" role="alert">
        <?php print $variables['success']; ?>
      </div>
    <?php endif; ?>
    <form action="" method="post" id="case">
      <strong>To submit a new case fill all necessary data below:</strong>
      <div class="form-group">
        <label for="surname">Surname</label>
        <input type="text" class="form-control" name="surname" value="<?php echo htmlspecialchars($variables['surname']);?>" placeholder="Enter your surname" required>
      </div>
      <div class="form-group">
        <label for="firstName">First name</label>
        <input type="text" class="form-control" name="firstName" value="<?php echo htmlspecialchars($variables['firstName']);?>" placeholder="Enter your first name" required>
      </div>
      <div class="form-group">
        <label for="lastName">Last name</label>
        <input type="text" class="form-control" name="lastName" value="<?php echo htmlspecialchars($variables['lastName']);?>" placeholder="Enter your last name" required>
      </div>
        <div class="form-group">
        <label for="birthdate">Birthdate</label>
        <input type="date" class="form-control" name="birthdate" value="<?php echo htmlspecialchars($variables['birthdate']);?>" placeholder="Enter your birthdate" required>
      </div>
      <div class="form-group">
        <label for="pSer">Personal document series</label>
        <input type="text" class="form-control" name="pser" value="<?php echo htmlspecialchars($variables['pser']);?>" placeholder="Enter your document series" required>
      </div>
      <div class="form-group">
        <label for="pNum">Personal document number</label>
        <input type="text" class="form-control" name="pnum" value="<?php echo htmlspecialchars($variables['pnum']);?>" placeholder="Enter your document number" required>
      </div>
      <input type="submit" name="casesFormSubmit" class="btn btn-primary">
    </form>
  </div>
</div>